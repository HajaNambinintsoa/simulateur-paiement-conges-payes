require 'date'
module FonctionsUtiles
    def calcul_jours_ouvrables(date_debut, date_fin)
        jours_ouvrables = 0
        date = date_fin
        while date >= date_debut
            jours_ouvrables = jours_ouvrables + 1 unless date.saturday? or date.sunday?
         date = date - 1
        end
        return jours_ouvrables
    end

    def calcul_nombre_jours_prorata (date_debut,date_fin)
        proratas = [0,0];
        # si la date de début n'est pas le début du mois il y a prorata
        if(date_debut.day > 1)
            proratas[0] = self.calcul_jours_ouvrables(date_debut, self.date_fin_mois(date_debut));
        end
        #si la date fin n'est pas la fin du mois il y a prorata
        if(date_fin.day < self.date_fin_mois(date_fin).day)
            proratas[1] = self.calcul_jours_ouvrables(Date.new(date_fin.year, date_fin.month, 1), date_fin);
        end
        return proratas;
    end
    
    def meme_mois_annee(date1,date2)
        date_1 = Date.new(date1.year,date1.month,1);
        date_2 = Date.new(date2.year,date2.month,1);
        return date_1 == date_2;
    end

    def date_fin_mois(date)
        date_fin_mois = Date.new(date.year,date.month,-1);
        return date_fin_mois;
    end

    def nombre_mois_avec_jours_complets(date_debut, date_fin)
        difference_mois = 0;
        if(date_debut.day > 1)
            difference_mois += 1;
        end
        if(date_fin == self.date_fin_mois(date_fin))
            date_fin = date_fin + 1;
        end
        return ((date_fin.year * 12 + date_fin.month) - (date_debut.year * 12 + date_debut.month))-difference_mois;
    end
end
