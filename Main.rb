require_relative 'CongeService'
require_relative 'FonctionsUtiles'
require_relative 'ModePaiement'

class Main
    include CongeService
    include FonctionsUtiles
  end

  main = Main.new();
  date1 =Date.new(2019,05,03);
  date2 =Date.new(2020,07,20);



# Paiement des congés chaque fin de période
p "paiement des congés chaque fin de periode #{main.paiementConge(date1,date2,2500,ModePaiement.new().chaque_fin_periode())}"
print "\n"
# Paiement congé chaque douze du mois
p "paiement des congés chaque douze du mois  #{main.paiementConge(date1,date2,2500,ModePaiement.new().decoupe_chaque_douze())}"
print "\n"
# Paiement congé chaque douze du mois
p "paiement des congés 10% plus regul  #{main.paiementConge(date1,date2,2500,ModePaiement.new().dix_pour_cent_regul())}"
print "\n"