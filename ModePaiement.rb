class ModePaiement
    CHAQUE_FIN_PERIODE = "Chaque fin de période"
    DECOUPE_CHAQUE_DOUZE = "Chaque douze du mois"
    DIX_POUR_CENT_REGUL = "Dix pour cent + regul"
    def chaque_fin_periode()
        return CHAQUE_FIN_PERIODE
    end
    def decoupe_chaque_douze()
        return DECOUPE_CHAQUE_DOUZE
    end
    def dix_pour_cent_regul()
        return DIX_POUR_CENT_REGUL
    end
  end