require 'date'
require_relative 'FonctionsUtiles'
require_relative 'ModePaiement'
module CongeService

    #Acquisition des congés
    def calcul_conges_acquis_periode (date_debut ,date_fin)
        quotient = 20;
        jour_de_conges = 2.5;
        limite_acquis = 259;
        maxCongesAcquis = 30.0;
        total_jours_ouvrables = self.calcul_jours_ouvrables(date_debut, date_fin);
        congeAcquis =  (total_jours_ouvrables <= limite_acquis) ? total_jours_ouvrables/quotient*jour_de_conges : maxCongesAcquis;
        return congeAcquis;
    end
    
    #calcul du salaire prorata
    def calcul_salaire_prorata(nombre_jours,salaire_brut_mensuel)
        return  nombre_jours*(salaire_brut_mensuel/22);
    end

    #salaire total pendant une période
    def salaire_total_pendant_periode(date_debut, date_fin, salaire_brut_mensuel)
        nombre_jours_prorata = self.calcul_nombre_jours_prorata(date_debut, date_fin);
        salaire_mois_travaille_sans_prorata = self.nombre_mois_avec_jours_complets(date_debut, date_fin)*salaire_brut_mensuel;
        return salaire_mois_travaille_sans_prorata+(self.calcul_salaire_prorata(nombre_jours_prorata[0],salaire_brut_mensuel))+(self.calcul_salaire_prorata(nombre_jours_prorata[1],salaire_brut_mensuel));
    end

    # montant des congés
    def calcul_montant_conges_payes_periode(date_debut , date_fin ,salaire_brut_mensuel)
        salaire_total_periode = salaire_total_pendant_periode(date_debut, date_fin, salaire_brut_mensuel);
        congesAcquis = self.calcul_conges_acquis_periode(date_debut, date_fin);
        #10% du salaire total
        montant_conge_paye = salaire_total_periode * 0.10;
        #maintien de salaire
        maintien_salaire = (salaire_brut_mensuel/22)*congesAcquis;
        if(montant_conge_paye < maintien_salaire)
            montant_conge_paye = maintien_salaire
        end
        return montant_conge_paye;
    end

    #calcul des périodes de référence durant un contrat
    def periodes_de_reference(date_debut_contrat, date_fin_contrat)
        periodes = [];
        date_debut_increment = date_debut_contrat;
        while date_debut_increment <= date_fin_contrat
            debut_periode_annee_en_cours = Date.new(date_debut_contrat.year,06,01);
            fin_periode_annee_en_cours = Date.new(date_debut_contrat.year,05,31);
            if(date_debut_increment == date_debut_contrat)
                # si le début du contrat est avant 31 Mai de l'année en cours
                if(date_debut_increment < fin_periode_annee_en_cours)
                    #si la date de fin de contrat après 31 Mai de l'année en cours
                    if(date_fin_contrat >= fin_periode_annee_en_cours)
                        periode_en_cours = [date_debut_contrat,fin_periode_annee_en_cours]
                        periodes << periode_en_cours
                        date_debut_increment = fin_periode_annee_en_cours
                        date_debut_contrat = fin_periode_annee_en_cours+1
                    #si la fin du contrat est avant le 31 Mai de l'année en cours, la période se termine sur le fin du contrat
                    else
                        periode_en_cours = [date_debut_contrat,date_fin_contrat]
                        periodes << periode_en_cours
                        break
                    end
                # sinon si le début du contrat est après 31 Mai de l'année en cours 
                elsif(date_debut_increment > fin_periode_annee_en_cours)
                    # si le fin du contrat est après le 31 Mai de l'année prochaine
                    if(date_fin_contrat >= fin_periode_annee_en_cours.next_year)
                        periode_en_cours = [date_debut_contrat,fin_periode_annee_en_cours.next_year]
                        periodes << periode_en_cours
                        date_debut_increment = fin_periode_annee_en_cours.next_year
                        date_debut_contrat = fin_periode_annee_en_cours.next_year+1
                        #si le fin du contrat est avant le 31 Mai de l'année prochaine la période se termine sur le fin du contrat
                        else
                            periode_en_cours = [date_debut_contrat,date_fin_contrat]
                            periodes << periode_en_cours
                        break
                    end
                end
            end
            date_debut_increment += 1;
        end
        return periodes
    end

    #affichage paie pour une période
    def salaire_par_mois_sur_une_periode(date_debut,date_fin,salaire_brut_mensuel,mode_de_paiement,conges_payes_periode_precendent,date_fin_contrat)
        mode_paiement = ModePaiement.new();
        salaires_par_mois = {};
        proratas = self.calcul_nombre_jours_prorata(date_debut,date_fin);
        nombre_mois_avec_jours_complets = self.nombre_mois_avec_jours_complets(date_debut,date_fin);
        montant_conge_periode_en_cours = self.calcul_montant_conges_payes_periode(date_debut,date_fin,salaire_brut_mensuel);
        decoupe_chaque_douze_total = 0;
        montant_conge_en_cours_temporaire = 0;
        douzaine = conges_payes_periode_precendent/12;
        date_a_parcourir = date_debut;
        dix_pour_cent_total = 0;
        # si le premier mois est en prorata
        if(proratas[0] != 0)
            valeur_prorata = self.calcul_salaire_prorata(proratas[0],salaire_brut_mensuel);
            dix_pour_cent_conge = 0;
            # 10% du salaire par mois
            if(mode_de_paiement == mode_paiement.dix_pour_cent_regul())
                dix_pour_cent_conge = valeur_prorata*0.10 ;
                dix_pour_cent_total += dix_pour_cent_conge;
                regularisation = montant_conge_periode_en_cours - dix_pour_cent_total;
                dix_pour_cent_conge += regularisation;
            end
            salaires_par_mois["#{date_debut.month}_#{date_debut.year}"] = self.calcul_salaire_prorata(proratas[0],salaire_brut_mensuel) + dix_pour_cent_conge;
            date_a_parcourir = date_a_parcourir.next_month
        end
        #les mois complets
        nombre_mois_avec_jours_complets.times{
            |i|
            montant_conge_en_cours_temporaire = 0;
            if(mode_de_paiement == mode_paiement.chaque_fin_periode())
                # ajout du congé au début du mois de Juin
                montant_conge_en_cours_temporaire = (proratas[0] == 0 && i == 0) ? conges_payes_periode_precendent : 0;
                #ajout de la valeur de congé à la fin du contrat
                montant_conge_en_cours_temporaire += (proratas[1] == 0 && i == nombre_mois_avec_jours_complets-1 && self.meme_mois_annee(date_a_parcourir,date_fin_contrat)) ? montant_conge_periode_en_cours : 0;
            end
            if(mode_de_paiement == mode_paiement.decoupe_chaque_douze())
                #reglement de congé par douzaine
                decoupe_chaque_douze_total += douzaine;
                montant_conge_en_cours_temporaire += douzaine;
                #reglement fin de contrat
                montant_conge_en_cours_temporaire += (proratas[1] == 0 && i == nombre_mois_avec_jours_complets-1 && self.meme_mois_annee(date_a_parcourir,date_fin_contrat)) ? conges_payes_periode_precendent - decoupe_chaque_douze_total + montant_conge_periode_en_cours : 0
            end
            if(mode_de_paiement == mode_paiement.dix_pour_cent_regul())
                dix_pour_cent = salaire_brut_mensuel*0.10;
                montant_conge_en_cours_temporaire += dix_pour_cent;
                dix_pour_cent_total += dix_pour_cent;
                if(proratas[1] == 0 && i == nombre_mois_avec_jours_complets-1 && self.meme_mois_annee(date_a_parcourir,date_fin))
                    regularisation = montant_conge_periode_en_cours - dix_pour_cent_total;
                    montant_conge_en_cours_temporaire += regularisation;
                end
            end
            salaires_par_mois["#{date_a_parcourir.month}_#{date_a_parcourir.year}"] = salaire_brut_mensuel + montant_conge_en_cours_temporaire;
            date_a_parcourir = date_a_parcourir.next_month
        } 
        #si il y a prorata pour le dernier mois
        if(proratas[1] != 0)
            prorata_fin = self.calcul_salaire_prorata(proratas[1],salaire_brut_mensuel);
            #reglement fin de contrat pour chaque fin de contrat
            reglement_conge_fin = (mode_de_paiement == mode_paiement.chaque_fin_periode()) ? montant_conge_periode_en_cours : 0; 
            #reglement fin de contrat pour chaque douze
            reglement_conge_fin += (mode_de_paiement == mode_paiement.decoupe_chaque_douze()) ? conges_payes_periode_precendent - decoupe_chaque_douze_total + montant_conge_periode_en_cours : 0;
            #regularisation fin de contrat
            if(mode_de_paiement == mode_paiement.dix_pour_cent_regul())
                dix_pour_cent_total += prorata_fin*0.10;
                regularisation = montant_conge_periode_en_cours - dix_pour_cent_total;
                reglement_conge_fin += dix_pour_cent_total
                reglement_conge_fin += regularisation
            end
            salaires_par_mois["#{date_fin.month}_#{date_fin.year}"] = prorata_fin + reglement_conge_fin;
        end
        return salaires_par_mois
    end

    #paiement des congés selon le mode de paiement
    def paiementConge(date_debut_contrat, date_fin_contrat, salaire_brut_mensuel, mode_de_paiement)
        mode_paiement = ModePaiement.new();
        periodes_de_reference = self.periodes_de_reference(date_debut_contrat,date_fin_contrat);
        salaires_par_periode = [];
        #chaque fin de période
        conges_payes_periode_precedent = 0;
        periodes_de_reference.each{
            |periode|
            salaires_par_periode << self.salaire_par_mois_sur_une_periode(periode[0],periode[1],salaire_brut_mensuel,mode_de_paiement,conges_payes_periode_precedent,date_fin_contrat)
            conges_payes_periode_precedent = self.calcul_montant_conges_payes_periode(periode[0],periode[1],salaire_brut_mensuel);
        }
        return salaires_par_periode;
    end
  end
